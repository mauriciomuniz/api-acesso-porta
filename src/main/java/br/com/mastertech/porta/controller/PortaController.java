package br.com.mastertech.porta.controller;

import br.com.mastertech.porta.DTO.PortaDTO;
import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.service.PortaService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    public PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta cadastrarPortaNoSistema(@RequestBody @Valid PortaDTO porta) {
        return portaService.cadastrar(porta);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Porta consultarPortaPorId(@PathVariable(name = "id") int id) throws NotFoundException {
        return portaService.consultar(id);
    }
}