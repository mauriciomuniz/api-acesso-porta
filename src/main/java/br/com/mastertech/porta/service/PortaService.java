package br.com.mastertech.porta.service;

import br.com.mastertech.porta.DTO.PortaDTO;
import br.com.mastertech.porta.model.Porta;
import br.com.mastertech.porta.repository.PortaRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    public PortaRepository portaRepository;

    public Porta cadastrar(PortaDTO portaDTO) {
        return moveEntity(portaDTO);
    }

    public Porta consultar(int id) throws NotFoundException {
        Optional<Porta> portaOptional = portaRepository.findById(id);
        if (portaOptional.isPresent()) {
            Porta porta = portaOptional.get();
            return porta;
        } else {
            throw new NotFoundException("Porta não encontrada.");
        }
    }

    public Porta moveEntity (PortaDTO portaDTO) {
        Porta porta = new Porta();
        porta.setAndar(portaDTO.getAndar());
        porta.setSala(portaDTO.getSala());
        return portaRepository.save(porta);
    }
}
